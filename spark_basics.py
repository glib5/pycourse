import pandas as pd

from pyspark.sql import SparkSession
import pyspark.sql.functions as sf

# make it explicit about what the "spark" object is
spark = SparkSession.getActiveSession()

# create a fake df
df = {"A": [1, 2, 3, 4], "B": [5.5, 6.6, 7.7, 8.8]}
df = pd.DataFrame(df)
df = spark.createDataFrame(df)

# print it to screen in nice format (on the platform notebook only)
df.display()


# a small pipe with common operations
example = (
    df
    # add a new col depending on existing ones
    .withColumn(
        # name
        "NEW_COL",
        # content
        sf.col("A") + sf.col("B") * 0.1
    )
    # create a new column with a single value in it (all rows have the same value)
    .withColumn("ONE", sf.lit(1))
    # rename a column (old_name, new_name)
    .withColumnRenamed("NEW_COL", "C")
    # discard a column
    .drop("ONE")
    # change a column in-place
    .withColumn("A", sf.col("A") % 2)
    # groupby
    .groupBy("A")
    # aggregate using the module containing spark functions
    # use the alias method to immediately give new names 
    .agg(
        sf.sum(sf.col("B")).alias("SUM_OF_B"),
        sf.min(sf.col("C")).alias("MIN_OF_C")
    )
    # compute a column conditionally on the value of other cols
    .withColumn(
        "L",
        sf
        # when(condition, value if condition is True)
        .when(sf.col("A")==1, "one")
        .when(sf.col("A")==0, "zero")
        # default value
        .otherwise("wrong case")
    )
    # filter
    # there is an identical function called "filter"
    # | == "or", & == "and"
    .where( (sf.col("A") == 0) | (sf.col("SUM_OF_B") > 14) )
)

example.display()

# hot to use an UDF (user defined function)

# write your function as a scalar function
# decorate it with the udf decorator from the pyspark.sql.functions module

@sf.udf
def add_one(i:int) -> int:
    return i + 1

# use the UDF to compute a new column
example = df.withColumn("A_PLUS_ONE", add_one("A"))

# NOTE: UDFs are considerably slower than "native" pyspark
# always try to express the logic using the (vast) functionality
# that the package gives you !

example.display()







