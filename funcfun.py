from datetime import date, timedelta
import functools
import itertools
import operator
import os
from typing import Any, Callable

import pandas as pd

# ============================================================================

def compute_x_indicator(x:float, y:float) -> float:
    z = x + y
    return x / z

# ============================================================================

def intro():
    nums = [1, 2, 3, 4, 5]

    cumulative_prod = functools.reduce(operator.mul, nums, 10)
    print(cumulative_prod)
    # (10) * [1*2, 3, 4, 5]
    # [2*3, 4, 5]
    # [6*4, 5]
    # [24*5]
    # 120

    cumulative_diff = functools.reduce(operator.sub, nums, 0)
    print(cumulative_diff)

# ============================================================================

def buy_food(name:str, amount:int) -> float:
    """generic buy food function"""
    # print(f"buying {amount} units of {name}")
    unit_prices = {
        "TOMATO": 0.6,
        "BANANA": 0.8,
        "KIWI": 1.2,
        "WATERMELON": 2.3
    }
    return unit_prices[name] * amount


def buy_food_from_shop(name:str, amount:int, shop:dict[str, float]) -> float:
    return shop[name] * amount


def shopping_example():
    prices = [
        buy_food("TOMATO", 4),
        buy_food("BANANA", 5),
        buy_food("WATERMELON", 1)
    ]
    print(f"you need to pay {sum(prices)}")

    # what if we want to fix one of the arguments
    # to get a more specific function ?
    prices = [
        buy_tomatoes(4),
        buy_bananas(5),
        buy_one("WATERMELON")
    ]
    print(f"you need to pay {sum(prices)}")


# homemade way
def buy_tomatoes(amount:int) -> float:
    return buy_food("TOMATO", amount)

# using 'partial': the result is the same as the above homemade
# example, but it's safer and faster
buy_bananas = functools.partial(buy_food, "BANANA")

buy_one = functools.partial(buy_food, amount=1)


# ============================================================================

cumprod = functools.partial(functools.reduce, operator.mul)
# cumprod([1, 2, 3]) # 6

# also, it interplays well with the rest of the standard library
# cumprod(range(2, 5)) # 24


# ============================================================================

# recursive function: a function that calls itself
def factorial(n:int) -> int:
    # this is a GUARD CLAUSE: it prevents
    # the function entering an infinite loop
    if n < 2:
        return 1
    # the recursive part gets executed until the guard clause is encountered
    return n * factorial(n-1)


def fibonacci(n:int) -> int:
    print(f"received input {n = }")
    if n < 3:
        return 1
    return fibonacci(n-1) + fibonacci(n-2)


# caching a function means storing results to reuse them
@functools.cache
def fibonacci_cached(n:int) -> int:
    print(f"[cached] received input {n = }")
    if n < 3:
        return 1
    return fibonacci_cached(n-1) + fibonacci_cached(n-2)

# NOTE: you can cache ONLY pure functions, meaning functions
# that are fully deterministic. caching is a mapping from inputs
# to outputs (the idea is to skip the
# expensive computation that happens in between the two),
# therefore if some randomness or side effect are involved in your
# function you can't cache it because at the same input corresponds
# always the same output -- not exactly what you want when
# generating random values ! -> you loose all the side effect

# NOTE: each recursive function has an analogue iterative algorithm,
# that is usually more efficient -- even if it's harder to implement

def factorial_but_iterative(n:int) -> int:
    base = 1
    for i in range(2, n+1):
        base *= i # base = base * i
    return base

## possible pandas application:
# makes sense to cache this if it's going to be applied to a column
# where there are lots of duplicates (GRADES), while for the col ID
# there will be no advantage (actually there is only the problem
# of more memory usage due to the cache creation !).

@functools.cache
def plus_one(n:int) -> int:
    return n + 1
# {1:2, 4:5}


@functools.cache
def new_exam_date(n:int, ref:date) -> date:
    return ref + timedelta(days=n)


def cache_for_pandas():
    df = pd.DataFrame()
    K = 10
    df["ID"] = list(range(K))
    grades  = itertools.cycle([1, 2, 3])
    df["GRADE"] = [next(grades) for _ in range(K)]

    df["NEW_GRADES"] = df["ID"].apply(plus_one)
    df["EXAM_DATE"] = df["NEW_GRADES"].apply(new_exam_date, ref=date.today())
    print(df)

# ============================================================================

# very *classic* case: the first functions most likely belong to a
# library that we use in our code

def go_to_next_first_of_month(d:date) -> date:
    if d.month == 12:
        return d.replace(year=d.year+1, month=1, day=1)
    return d.replace(month=d.month+1, day=1)


@functools.cache
def advance_one_month(d:date) -> date:
    new = go_to_next_first_of_month(d)
    return _account_for_start_date(d.day, new)


def advance_n_months(d:date, n:int) -> date:
    new = d
    for _ in range(n):
        new = advance_one_month(new)
    return _account_for_start_date(d.day, new)


def _account_for_start_date(original_day:int, new:date) -> date:
    while 1:
        if original_day == 0:
            raise Exception("bad date")
        try:
            return new.replace(day=original_day)
        except ValueError:
            original_day -= 1


# this function is a particular case of the above one:
# therefore it's ok if it's not in the library
# however, in our code, we only consider semesters, so
# this is all we need: time to use functools.partial !

# we are just fixing one argument, but since the parameter names
# in the library were not so clear (*classic* case again: 'months_to_advance'
# would be have been better than 'n', but we got that ...)

# 'partial' makes out code clearer -> it's easy to read 20
# times 'advance_six_months(my_date)' rather than reading
       #'advance_n_months(my_date, 6)'.

# from datelib import advance_n_months

advance_six_months = functools.partial(advance_n_months, n=6)

# if needed, it can also be re-cached (at a higher level)
advance_six_months_cached = functools.cache(advance_six_months)

# ============================================================================

ComposableFunction = Callable[[Any], Any]




def compose(*functions: ComposableFunction) -> ComposableFunction:
    return functools.reduce(lambda f, g: lambda x: g(f(x)), functions)


def compose2(*functions):
    return functools.reduce(lambda f, g: lambda x: g(f(x)), functions)


# next_factorial = compose(plus_one, factorial_but_iterative)
# nth_fibonacci = compose(next_factorial, fibonacci_cached)

nth_fibonacci = compose(plus_one, factorial_but_iterative, fibonacci_cached)



def compose_example():
    num = 3

    x = plus_one(num)
    x = factorial_but_iterative(x)
    x = fibonacci_cached(x)
    print(x)
    # x = fibonacci_cached(factorial_but_iterative(plus_one(num))) #

    y = nth_fibonacci(num)
    print(y)

# ============================================================================

if __name__ == "__main__":
    _ = os.system("cls")

    # print(factorial(5))
    # print(fibonacci(10))
    # print(fibonacci_cached(10))

    # shopping_example()

    # intro()

    # print(cumprod([2, 3, 2]))

    # print(date.today())
    # print(advance_six_months(date.today()))
    # x = date(2022, 1, 31)
    # print(x)
    # print(advance_six_months(x))
    # x = date(2022, 4, 30)
    # print(x)
    # print(advance_six_months(x))

    # print(cache_for_pandas())

    # compose_example()


