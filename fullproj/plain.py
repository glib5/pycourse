import datetime
import os
import pandas as pd

analysis_date = "2023-08-29"
if datetime.datetime(int(analysis_date[:4]), int(analysis_date[5:7]), int(analysis_date[9:])).weekday() > 4:
    print("We don't run this code on weekends !")
    quit()

folder = r"M:\%5C\temp\guido\pycourse\fullproj"
output_folder = os.path.join(folder, f"outputs/{analysis_date}")
if not os.path.exists(output_folder):
    os.mkdir(output_folder)

# load data
df1 = pd.read_csv(os.path.join(folder, "inputs/data1.csv"))
df2 = pd.read_csv(os.path.join(folder, "inputs/data2.csv"))

# aggregate data
all_data = pd.merge(df1, df2, how="left", on=["NAMES", "AGES"])
all_data = all_data.dropna()
all_data.columns = [col[:-1] if col.endswith("S") else col for col in all_data.columns]

# produce mean by age
result = all_data.groupby(["NAME"], as_index=False)
result = result.agg(GPA_MEAN_BY_NAME = ("GPA", "mean"))
#result.to_csv(os.path.join(output_folder, "gpa_mean_by_name.csv"), index=False)
print("gpa_mean_by_name.csv saved")

# produce gpa statistics
result = all_data[all_data["NAME"].isin(("Alice", "Caroline", "Erica"))]
result = result[["NAME", "GPA"]]
result["MIN_GPA_BY_NAME"] = result.groupby("NAME")["GPA"].transform("min")
result["MEAN_GPA_BY_NAME"] = result.groupby("NAME")["GPA"].transform("mean")
result["DELTA_BETWEEN_MEAN_AND_MIN"] = result["MEAN_GPA_BY_NAME"] - result["MIN_GPA_BY_NAME"]
result = result.sort_values(by=["NAME", "GPA"], ascending=[True, False]).reset_index(drop=True)
result.to_csv(os.path.join(output_folder, "gpa_statistics.csv"), index=False)
print("gpa_statistics.csv saved")

print("all ok")