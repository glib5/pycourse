from pathlib import Path

import pandas as pd


def make_singular(s:str) -> str:
    if s.endswith("S"):
        return s[:-1]
    else:
         return s


def main():
    analysis_date = pd.Timestamp.today()
    if analysis_date.weekday() > 4:
        print("We don't run this code on weekends !")
        quit()

    root_folder = Path(r"M:\%5C\temp\guido\pycourse\fullproj")
    inputs = root_folder / "inputs"
    input1 = inputs / "data1.csv"
    input2 = inputs / "data2.csv"
    outputs = root_folder / "outputs"

    output_folder = outputs / analysis_date.strftime("%Y-%m-%d") 
    output_folder.mkdir(parents=True, exist_ok=True)

    # load data
    df1 = pd.read_csv(input1)
    df2 = pd.read_csv(input2)
    # combine data
    all_data = (
        df1
        .merge(
            right=df2,
            how="left",
            on=["NAMES", "AGES"]
        )
        .dropna()
        .rename(columns=make_singular)
    )

    out1 = output_folder / "gpa_mean_by_name.csv"
    (
        all_data
        .groupby(["NAME"], as_index=False)
        .agg(GPA_MEAN_BY_NAME = ("GPA", "mean"))
        .to_csv(out1, index=False)
    )
    print("gpa_mean_by_name.csv saved")

    out2 = output_folder / "gpa_statistics.csv"
    good_names = ["Alice", "Caroline", "Erica"]
    (
        all_data
        .query("NAME in @good_names")
        .filter(items=["NAME", "GPA"])
        .assign(
            MIN_GPA_BY_NAME = lambda df: df.groupby("NAME")["GPA"].transform("min"),

            MEAN_GPA_BY_NAME = lambda df: df.groupby("NAME")["GPA"].transform("mean"),

            ONE = 1
        )
        .assign(DELTA_BETWEEN_MEAN_AND_MIN = lambda df: df["MEAN_GPA_BY_NAME"] - df["MIN_GPA_BY_NAME"])
        .sort_values(by=["NAME", "GPA"], ascending=[True, False])
        .reset_index(drop=True)
        .to_csv(out2, index=False)
    )
    print("gpa_statistics.csv saved")
    print("all ok")


if __name__ == "__main__":
    main()