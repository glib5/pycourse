import concurrent.futures as cf
from dataclasses import dataclass
from datetime import date
from pathlib import Path

import pandas as pd


class AnalysisError(Exception):
    """custom exception"""


@dataclass(frozen=True)
class ProjDir:
    """container with high-level paths information"""
    folder = Path(__file__).parent    
    inputs = folder / "inputs"
    data1 = inputs / "data1.csv"
    data2 = inputs / "data2.csv"
    outputs = folder / "outputs"


def check_weekday() -> date:
    """
    check if it's a good day to run the code 
    if not, raise error
    """
    today = date.today()
    if today.weekday() > 4:
        raise AnalysisError("We don't run this code on weekends !")
    return today


def create_output_folder(day:date) -> Path:
    """builds the specific output folder"""
    folder = ProjDir.outputs / day.strftime("%Y-%m-%d")
    if not folder.exists():
        folder.mkdir()
    return folder


def load_data() -> pd.DataFrame:
    # load all data sources
    with cf.ThreadPoolExecutor() as pool:
        source1_handle = pool.submit(pd.read_csv, ProjDir.data1)
        source2_handle = pool.submit(pd.read_csv, ProjDir.data2)
    # aggregate them
    return (
        source1_handle
        .result()
        .merge(
            right=source2_handle.result(),
            on=["NAMES", "AGES"],
            how="left"
        )
        .dropna()
        .rename(columns=lambda s: s[:-1] if s.endswith("S") else s)
    )


def produce_mean_by_age(folder:Path, df:pd.DataFrame):
    f = folder / "gpa_mean_by_name.csv"
    (
        df
        .groupby(by=["NAME"], as_index=False)
        .agg(
           GPA_MEAN_BY_NAME = ("GPA", "mean")
        )
        .to_csv(f, index=False)
    )
    print(f"{f.name} saved")


def produce_gpa_statistics(folder:Path, df:pd.DataFrame):
    f = folder / "gpa_statistics.csv"
    good_names = ("Alice", "Caroline", "Erica")
    (
        df
        .query("NAME in @good_names")
        .drop(columns=["AGE"])
        .assign(
            MIN_GPA_BY_NAME = lambda df: df.groupby(by=["NAME"])["GPA"].transform("min"),
            MEAN_GPA_BY_NAME = lambda df: df.groupby(by=["NAME"])["GPA"].transform("mean"),
        )
        .assign(
            DELTA_BETWEEN_MEAN_AND_MIN = lambda df: df["MEAN_GPA_BY_NAME"] - df["MIN_GPA_BY_NAME"]
        )
        .sort_values(by=["NAME", "GPA"], ascending=[True, False])
        .reset_index(drop=True)
        .to_csv(f, index=False)
    )
    print(f"{f.name} saved")


def perform_analysis(folder:Path, df:pd.DataFrame):
    produce_mean_by_age(folder, df)
    produce_gpa_statistics(folder, df)

# ============================================================================

import tempfile
from typing import Callable

def _pkl_to_df_gate(func:Callable[[Path, pd.DataFrame], None], inpt:Path, folder:Path):
    return func(folder, pd.read_pickle(inpt))


def perform_analysis_faster(df:pd.DataFrame, folder:Path):
    with tempfile.TemporaryDirectory() as tdir:
        temp_data = Path(tdir) / "data.pkl"
        df.to_pickle(temp_data)
        with cf.ProcessPoolExecutor() as pool:
            pool.submit(_pkl_to_df_gate, produce_mean_by_age, temp_data, folder)
            pool.submit(_pkl_to_df_gate, produce_gpa_statistics, temp_data, folder)

# ============================================================================

def main():
    today = check_weekday()
    current_folder = create_output_folder(today)
    data = load_data()
    perform_analysis(current_folder, data)
    # perform_analysis_faster(data, current_folder)


if __name__ == "__main__":
    main()
