import concurrent.futures as cf
from pathlib import Path
import random
import time

import pandas as pd

# there are many modules for doing this
#
# this one is in the standard library and has basically anything we need
#
# CLASSIC CODE: one thing at a time (one cashier, one line)
# CONCURRENCY: *dealing* with many things at once (one cashier, 2 lines)
# PARALLELISM: *doing* many things at once (2 cashiers, 2 lines)
#
# concurrent code is good for I/O operations (reading/writing to
# files, dbs, web, ...); i.e. where there is a lot of waiting to do
#
# parallel code is good when there are lots of computations to be done
#
# DON'T use them togheter
# use pool objects
#
# MAP: apply the same operation to different inputs
# SUBMIT: execute that single task
#
# parallel code in spyder does NOT work, concurrent does
#
# passing large objects (like DataFrames !) across different parallel
# processes is quite costly.
#
# https://www.youtube.com/watch?v=X7vBbelRXn0


print("*** file started ***")

# =============================================================================

def double(x:int) -> int:
    return x + x


def triple(x:int) -> int:
    return x * 3

# =============================================================================

def with_statement_explained():
    # in programming, there is often the need for using resources that
    # need to be managed: this means
    # 1) open the resource
    # 2) use it
    # 3) close it
    # classic examples: files, db connections, etc.

    my_resource = Path(r"M:\%5C\temp\guido\pycourse\iris.csv")

    # usual way:
    f = my_resource.open()
    txt = f.read()
    f.close()

    # the python way
    with my_resource.open() as f:
        txt = f.read()
    # f is automatically closed here, even in case of exceptions/problems

    # since python is so high-level, sometimes you get all in 1 go:
    txt = my_resource.read_text()
    # as well as
    df = pd.read_csv(my_resource)

# ============================================================================

def do_task(taks_name:str, time_needed:int):
    print(f"{time.ctime()} | '{taks_name}' started (waiting {time_needed} seconds)")
    time.sleep(time_needed)
    print(f"{time.ctime()} | '{taks_name}' ended")


def real_world_example():
    tasks = ["bake cake", "read mail", "do laundry"]
    times = [3, 1, 2]

    # sequential
    for task, timing in zip(tasks, times):
        do_task(taks_name=task, time_needed=timing)

    classic_map = map(do_task, tasks, times)

    print()
    # concurrent
    with cf.ThreadPoolExecutor() as pool:
        pool.map(do_task, tasks, times)

# =============================================================================

def submit_example():

    with cf.ThreadPoolExecutor() as pool:
        handle_double = pool.submit(double, 3)
        handle_triple = pool.submit(triple, 4)
    tripled = handle_triple.result()
    print(f"{tripled = }")
    doubled = handle_double.result()
    print(f"{doubled = }")


def tpool(nums:list[int]):
    with cf.ThreadPoolExecutor() as pool:
        doubles = pool.map(double, nums)
    print(doubles)
    print(list(doubles))


def ppool(nums:list[int]):
    with cf.ProcessPoolExecutor(max_workers=3) as pool:
        doubles = pool.map(double, nums)
    print(doubles)
    print(list(doubles))

# =============================================================================

def fuzz():
    time.sleep(random.random())


def fuzzprint(i:int):
    fuzz()
    print(f"the values is {i}")


def race_condition_example():
    # classic way
    for i in range(5):
        fuzzprint(i)
    print()

    # concurrent way
    with cf.ThreadPoolExecutor() as pool:
        pool.map(fuzzprint, range(5))
    print()


def save_subset(iris_file, species, data):
    subset_file = iris_file.with_stem(species)
    print(subset_file.stem)
    subset = data[data["species"]==species]
    subset.to_csv(subset_file, index=False)


import itertools


def show_and_tell():
    iris_file = Path(r"M:\%5C\temp\guido\pycourse\ddd").with_name("iris.csv")
    data = pd.read_csv(iris_file)
    species = data["species"].unique().tolist()

    # from sequential to simultaneous
    for s in species:
        subset_file = iris_file.with_stem(s)
        print(subset_file.stem)
        subset = data[data["species"]==s]
        subset.to_csv(subset_file, index=False)

    for s in species:
        save_subset(iris_file, s, data)

    res = map(
        save_subset,
        itertools.repeat(iris_file),
        species,
        itertools.repeat(data)
    )

    with cf.ThreadPoolExecutor() as pool:
        res = pool.map(
            save_subset,
            itertools.repeat(iris_file),
            species,
            itertools.repeat(data)
        )


# =============================================================================

if __name__ == "__main__":
    nums = [1, 2, 3, 4]
    # print()
    # print(nums)
    # print()
    # tpool(nums)
    print()
    ppool(nums)
    print()
    # submit_example()
    # print()
    # with_statement_explained()
    # print()
    # race_condition_example()
    # real_world_example()
    # print()
    # show_and_tell()
    print("\n *** all good ***")
