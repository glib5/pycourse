from pathlib import Path

import pandas as pd
import polars as pl

# here we use polars because not everybody may have access to a spark cluster

# functioning and syntax between polars and pyspark overlaps most of the time.


def eager_vs_lazy(iris_file:Path):
    # difference between eager and lazy evaluation:

    # pandas is eager: every operation
    # is fully performed as soon as  it's requested

    # polars is lazy: before actually perform any operation,
    # the full pipeline is taken into account and the shortest path to the input is selected
    # (polars also supporst an eager mode but that will void all the advantage ...)

    # example
    eager_max_sepal_length = (
        pd
        .read_csv(iris_file)
        ["sepal_length"]
        .max()
    )
    print(eager_max_sepal_length)

    lazy_max_sepal_length = (
        pl
        .scan_csv(iris_file)
        .select(pl.col("sepal_length"))
        .max()
        # .collect()
    )
    print(lazy_max_sepal_length.explain(optimized=True))
    print(lazy_max_sepal_length.collect())


def eager_vs_lazy_detail(iris_file:Path):

    print("PANDAS")
    panda = pd.read_csv(iris_file)
    print(panda)
    print()
    panda = panda["sepal_length"]
    print(panda)
    print()
    panda = panda.max()
    print(panda)
    print()

    print("BEAR")
    bear = pl.scan_csv(iris_file)
    print(bear.explain(optimized=True))
    print()
    bear = bear.select(pl.col("sepal_length"))
    print(bear.explain(optimized=True))
    print()
    bear = bear.max()
    print(bear.explain(optimized=True))
    print()



def pipeline_example(iris_file:Path):
    a = (
        pl
        .scan_csv(iris_file)
        .unique() # drop duplicates
        .select(
            pl.col("sepal_length"),
            pl.col("sepal_width"),
            pl.col("species")
        )
        .with_columns(
            pl.col("sepal_length").add(pl.col("sepal_width")).alias("indicator")
        )
        .filter( pl.col("sepal_width").gt(pl.lit(3.0)) )
        .with_columns(
            pl
            .when(pl.col("species")=="setosa").then(pl.lit(1))
            .when(pl.col("species")=="virginica").then(pl.lit(2))
            .when(pl.col("species")=="versicolor").then(pl.lit(3))
            .otherwise(pl.lit(4))
            .alias("species_order")
        )
        .sort(by=pl.col("species_order"))
        .groupby(by=pl.col("species"))
        .agg(
            pl.col("sepal_length").mean().alias("mean_sepal_length"),
            pl.col("sepal_width").max().alias("max_sepal_width"),
        )
        .rename({"species": "SPECIES"})
    )
    print(a.collect())
    print(a.collect().to_pandas())


def main():
    # iris_file = Path(__file__).with_name("iris.csv")
    iris_file = Path(r"M:\%5C\temp\guido\pycourse\zzz.csv").with_name("iris.csv")
    eager_vs_lazy(iris_file)
    eager_vs_lazy_detail(iris_file)
    pipeline_example(iris_file)


if __name__ == "__main__":
    main()
