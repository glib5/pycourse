from dataclasses import dataclass
from datetime import date
from enum import Enum, auto
import random
from typing import Any

# from pathlib import Path

# how to define an object of any kind and
# the most common features.
# note that defining a class means to create a
# brand NEW datatype
class Order:

    # this is MANDATORY to define which pieces of data belong to out object
    # since it's a function, it can have default values
    def __init__(self, name:str, unit_price:float, quantity:int=1) -> None:
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    # this is a METHOD, i.e.: a function 'belonging' to
    # the class: only objects of this type can use it

    # the first argument is ALWAYS 'self' and refers to
    # the object itself. the others follow the usual
    # rules for function argument definitions
    def change_quantity(self, new_quantity:int) -> None:
        # by using 'self' we have access to any piece of
        # data stored inside the object

        # this method specifically, modfies a piece of the object
        self.quantity = new_quantity

    # a property is like a method, but takes only self as
    # a parameter and it's supposed to be simple (one line only !)
    @property
    def total_cost(self) -> float:
        return self.unit_price * self.quantity

    # this is another method, that returns something.
    # you can assign the result of using this method
    # to a variable as you would do with a simple function
    def as_dict(self) -> dict[str, Any]:
        return {
            "name": self.name,
            "price": self.unit_price,
            "quantity": self.quantity
        }

    # a classmethod is a method whose first argument is always
    # 'cls': it's normally used to provide alternative
    # ways of creating an object of the class
    @classmethod
    def from_dict(cls, d:dict[str, Any]):
        return cls(
            d["name"],
            d["price"],
            d["quantity"]
        )

    # a static method it's just a function
    # we place it inside the object  because maybe it's not
    # general enonugh to be a regular function
    # note how there is neither 'self' nor 'cls' in the arguments
    @staticmethod
    def double(n:int) -> int:
        return n*2

# above we have seen the class, i.e.: the blueprint

# when we create one object, we say we have an INSTANCE of the class,
# like as follows

# you can think of this as calling the above "__init__" function
my_order = Order(
    "bananas",
    0.80,
    4
)

# access single pieces of data, uniques to the instance,
# with the 'dot' notation
print(my_order.name) # they are called 'attributes'
print(my_order.total_cost) # total_cost is a property, but looks like an attribute

# how to use a method: note that we do NOT provide the 'self' argument
# also, in this case, since the method does not return anything, we don't
# need to assign the value of the operation to any new variable
my_order.change_quantity(2)

print(my_order)
# this method returns something, so i store the
# result in 'x', like with a normal function
x = my_order.as_dict()
print(x)

# here we use a classmethod
second_order = Order.from_dict({
    "name": "apples",
    "price": 0.70,
    "quantity": 3
})

# here we use a staticmethod
print(Order.double(8))

# note that both class- and static- method don't need
# an instance to be used, you can call the class directly

# -----------------------------------

# the dataclass function (from the dataclasses module)
# allows for a different way of building classes
# useful and clearer when you have many attributes

class Character_1:

    def __init__(self, name:str, movie:str, is_good:bool) -> None:
        self.name = name
        self.movie = movie
        self.is_good = is_good


@dataclass
class Character_2:
    name:str
    movie:str
    is_good:bool


c1 = Character_1("Spiderman", "spiderman 3s", True)

c2 = Character_2("Batman", "the Dark Knight", True)

print(c1.is_good)
print(c2.movie)

# -----------------------------------

# abstraction, encapsulation, inheritance, polymorphism


# abstraction and encapsulation: by using classes,
# implementation details can be hidden and only
# 'high' level functions are used
class Car:

    def drive(self):
        # some heavy code here
        # ...
        print("i am driving")


car = Car()
car.drive()


# ----

# inheritance: a way to reuse code
# by creating a class after inheriting from another,
# the 'child' will be able to do all the parent
# was able to do

class Person:

    def __init__(self, name:str, age:int) -> None:
        self.name = name
        self.age = age

    def show_name(self):
        print(self.name)

    def introduce(self):
        print(f"{self.name=}, {self.age=}")


class Student(Person):

    def __init__(self, name: str, age: int, school:str) -> None:
        super().__init__(name, age)
        self.school = school

    def introduce(self):
        print(f"{self.name=}, {self.age=}, {self.school=}")


a_person = Person("al", 33)
a_student = Student("bob", 25, "marie curie")

# plymorphism: all things in this list know how to
# 'show name' and 'introduce' themselves, but everyone
# does it in their unique way, depending on the class
# they belong to
entities = [a_person, a_student]
for entity in entities:
    entity.show_name()
    entity.introduce()

# -------------------------------------

# good inheritance: ABC, namedtuples, Enums, adding some methods (pay attention !)

# here we simply extend a bit the functionality of the 'date' object
class Date(date):

    def quarter(self) -> int:
        """
        returns the quarter of the year [1-4]
        """
        return int(self.month // 3.1) + 1

# --------------------------------------

# inheritance can be difficult
# MRO, multiple inheritance, long inheritance trees

# personally: never go beyond 1 level of inheritance

# why subclassing a dataframe is not recommendable ?

# --------------------------------------

# compsition is better than inheritance -> helps de-coupling

# all info in one place
@dataclass
class CreditCard:
    number:int
    owner_name:str
    owner_age:int
    is_valid:bool
    adresse:str

base_card = CreditCard(
    123,
    "alan",
    29,
    True,
    "rue blabla"
)

# info is in different places, where it logically belongs
# note that this way our classes are smaller and less error-prone
@dataclass
class Person:
    name:str
    age:int
    adresse:int

@dataclass
class Card:
    owner:Person
    number:int
    is_valid:bool

card_owner = Person("bob", 28, "rue rivoli")
nice_card = Card(card_owner, 456, True)



# in both cases the same info is available when using a 'card'
# but in the second one it's more structured

print(base_card.owner_age)

print(nice_card.owner.age)



# --------------------------------

# since python is built on objects, when building one
# we have access to very deep parts of the program

# these kind of methods are usually known as 'dunder'
# or 'magic' and are signalled by double underscores before
# and after their name


class BadNumber:

    def __init__(self, value:int) -> None:
        self.value = value

    # here we modify what the '+' sign does when using objects of this kind
    def __add__(self, other):
        if not isinstance(other, BadNumber):
            raise TypeError("can't add BadNumber and non-BadNumber")
        return BadNumber(self.value - other.value)

    def __repr__(self) -> str:
        return str(self.value)


bad_num_a = BadNumber(5)
bad_num_b = BadNumber(3)

res = bad_num_a + bad_num_b
print(f"{bad_num_a} + {bad_num_b} = {res}") # 5 + 3 = 2

# note that we did NOT broke math, we just defined a new 'algebra'

# how does this explain pathlib / path concatenation ?

# ---------------------------------------------------------

# enums: for types with a limited possible number of values

class Role(Enum):
    PRESIDENT = auto()
    VICEPRESIDENT = auto()
    MANAGER = auto()
    OFFICER = auto()
    TRAINEE = auto()


@dataclass
class Employee:
    name: str
    age: int
    salary: int
    role: Role


boss = Employee(
    "alan",
    55,
    20_000,
    Role.PRESIDENT
)


# -----------------------------------------

# state-oriented vs data-oriented classes


class MarioState(Enum):
    KO = auto()
    SMALL = auto()
    BIG = auto()
    FIREFLOWER = auto()


class Enemy:
    def __repr__(self) -> str:
        return "enemy"

class Mushroom:
    def __repr__(self) -> str:
        return "mushroom"

class FireFlower:
    def __repr__(self) -> str:
        return "fireflower"


class Mario:
    def __init__(self) -> None:
        self.state = MarioState.SMALL

    def touches(self, obj):
        if isinstance(obj, Enemy):
            self._handle_enemy()
        elif isinstance(obj, Mushroom):
            self._handle_mushroom()
        elif isinstance(obj, FireFlower):
            self._handle_fireflower()
        else:
            raise TypeError()

    def _handle_enemy(self):
        if self.state == MarioState.SMALL:
            self.state = MarioState.KO
        elif self.state == MarioState.BIG:
            self.state = MarioState.SMALL
        elif self.state == MarioState.FIREFLOWER:
            self.state = MarioState.BIG

    def _handle_mushroom(self):
        if self.state == MarioState.SMALL:
            self.state = MarioState.BIG

    def _handle_fireflower(self):
        if self.state in (MarioState.SMALL, MarioState.BIG):
            self.state = MarioState.FIREFLOWER


def mario_game():
    events = random.choices(
        population=[FireFlower(), Enemy(), Mushroom()], 
        weights=[0.33, 0.27, 0.40],
        k=10
    )
    mario = Mario()
    for token in events:
        print(f"a {mario.state} touches {token}")
        mario.touches(token)
        print(f"state is now: {mario.state}\n")
        if mario.state == MarioState.KO:
            print("GAME OVER, MAMMA MIA !")
            break
    else:
        print("YOU WIN !")

# mario_game()

# possible output:

# a MarioState.SMALL touches mushroom
# state is now: MarioState.BIG

# a MarioState.BIG touches enemy
# state is now: MarioState.SMALL

# a MarioState.SMALL touches fireflower
# state is now: MarioState.FIREFLOWER

# a MarioState.FIREFLOWER touches enemy
# state is now: MarioState.BIG

# a MarioState.BIG touches enemy
# state is now: MarioState.SMALL

# a MarioState.SMALL touches enemy
# state is now: MarioState.KO

# GAME OVER, MAMMA MIA !

# -----------------------

# is a dataframe data-oriented or state-oriented ?

# real-world-code examples:
# M:\%5C\temp\guido\dst_projects\order_book
# M:\%5C\temp\guido\dst_projects\spark_merge
